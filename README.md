# CS251 Outlab 9

At the request of a small section of the class, some starter code is being
provided. It took the TAs not much time to hack this using the official
[`RecyclerView`][official] sample code.

Steps to reproduce:
1. Start a new Android project from the Navigation drawer template.
2. Copy some code (piece by piece) from [the repo][official].
   - Modify till it compiles.
3. Profit.

## Instructions
You must declare in your submission if you used this repo (or if you were
inspired from it). You are free to use it as a starting point. **There will be
no penalty for using this.**

Plan (for you guys):
1. Write the DB layer.
2. Write more activities.
3. Remove unecessary stuff from layout.
4. Test and submit.

[official]: https://github.com/android/views-widgets-samples/tree/master/RecyclerView
