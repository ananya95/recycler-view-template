package com.cs251.ta.something;

/* Models the SubTask Entity */
public class SubTaskItem {
    public String mName, mDescription;

    SubTaskItem(String name, String description){
        mName = name;
        mDescription = description;
    }
}
